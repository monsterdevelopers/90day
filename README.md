# scraper

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### generate main css
$ node-sass -r -w src/assets/scss -o src/assets/css --output-style compressed