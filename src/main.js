import '@babel/polyfill'
import Vue from 'vue'
import Vuetify from 'vuetify'
import './plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from './App.vue'

Vue.use(Vuetify)

require('../src/assets/css/main.css')

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
